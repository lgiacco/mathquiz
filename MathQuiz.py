import random

def LoadRecords(nom):
    sc = 0
    att = 0
    succ = 0
    try:
        with open("math.sav", "r") as file:
            for record in file.readlines():
                player = record.split(", ")
                if player[0] == nom:
                    sc = int(player[1])
                    att = int(player[2])
                    succ = int(player[3])
                    print("Player " + nom + " loaded")
                    print("Score: " + player[1])
                    print("Attempts: " + player[2])
                    print("Correct guesses: " + player[3])
    except FileNotFoundError:
        file = open("math.sav", "w")
        file.close()
        
    pS = [nom, sc, att, succ]
    return pS

def SaveRecords(nom):
    with open("math.sav", "r+") as file:
        separator = ", "
        records = file.readlines()
        for record in records:
            player = record.split(separator)
            if player[0] == nom:
                records.remove(record)

        records.append(name + separator + str(score) + separator + str(attempts) + separator + str(corrects) + "\n")

        file.seek(0)
        file.truncate(0)
        file.writelines(records)


name  = input("What is your name? ")

exit = False

playerStruct = LoadRecords(name)

score = playerStruct[1]
attempts = playerStruct[2]
corrects = playerStruct[3]
operatorArray = ["+", "-", "*", "/"]

print("\nWelcome " + name)

while exit == False:
    number1 = random.randint(0, 100)
    number2 = 0
    while number2 == 0:
        number2 = random.randint(0, 100)
    operator = random.randint(0, 3)

    if operator == 0:
        result = number1 + number2
    elif operator == 1:
        result = number1 - number2
    elif operator == 2:
        result = number1 * number2
    else:
        result = number1 / number2

    answer = input("\nWhat is: " + str(number1) + " " + operatorArray[operator] + " " + str(number2) + "? ")
    attempts += 1
    try:
        if float(result) == float(answer):
            score += 2
            corrects += 1
            print("\nYes, you got it!")
        else:
            score -= 1
            print("\nNo, sorry, the correct answer was " + str(result))
    except ValueError:
        if answer == "bye" or answer == "":
            attempts -= 1
            exit = True
        else:
            score -= 1
            print("\nNo, sorry, the correct answer was " + str(result))

    print("Your current score is " + str(score))
    print("Attempts: " + str(attempts))
    print("Correct guesses: " + str(corrects))

print("\nData saved")
print("Goodbye " + name)

SaveRecords(name)
